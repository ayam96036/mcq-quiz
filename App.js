import { Container } from "react-bootstrap";
import "./App.css";

import FormComponent from "./Components/Form/FormComponent";
function App(props) {
  return (
    <Container className="App ">
      <FormComponent name={props.name} />
    </Container>
  );
}

export default App;
