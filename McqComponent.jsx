import React, { useState } from "react";
import { mcqSet } from "./../../mcq";
export default function McqComponent(props) {
  // state has curent question
  const [currentQuestion, setCurrentQuestion] = useState(0);
  //   state to show score if student finished his quiz
  const [showScore, setShowScore] = useState(false);
  //   state to count how many questions is right
  const [score, setScore] = useState(0);
  // function to get next question and count how many questions is right if student was finishing show his score
  const handleAnswerOptionClick = (isCorrect) => {
    if (isCorrect) {
      setScore(score + 1);
    }

    const nextQuestion = currentQuestion + 1;
    if (nextQuestion < mcqSet.length) {
      setCurrentQuestion(nextQuestion);
    } else {
      setShowScore(true);
    }
  };

  return (
    <div>
      {showScore ? (
        <div className="score-section text-capitalize">
          {/* finaly his score */}
          You scored {score} out of {mcqSet.length}
        </div>
      ) : (
        <div>
          <div className="question-section">
            <div className="question-count text-capitalize">
              {/* questions was answered */}
              <span>Question No {currentQuestion + 1}</span> Out of
              {mcqSet.length}
            </div>
            <div className="question-text">
              {mcqSet[currentQuestion].questionText}
            </div>
          </div>
          <div className="answer-section">
            {/* map to get 4 choise  */}
            {mcqSet[currentQuestion].answerOptions.map((answerOption) => (
              <button
                className="my-1"
                onClick={() => handleAnswerOptionClick(answerOption.isCorrect)}
              >
                {answerOption.answerText}
              </button>
            ))}
          </div>
        </div>
      )}
    </div>
  );
}
