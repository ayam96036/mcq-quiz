import React, { useState } from "react";
import { Alert, Button } from "react-bootstrap";
import McqComponent from "../McqComponent/McqComponent";
export default function FormComponent(props) {
  // state to student name
  const [name, setName] = useState();
  // state to hidden form and show quiz if the student enter his name
  const [show, setShow] = useState(true);
  //   state if student dont enter his name or use !@#$%^&*()
  const [errMsg, setErrMsg] = useState("");

  return (
    <div className="my-auto">
      {show ? (
        <div>
          <form onSubmit={(event) => event.preventDefault()}>
            <input
              type="text"
              placeholder="Enter Your Name"
              value={name}
              onChange={(e) =>
                e.target.value.match("^[a-zA-Z ]*$")
                  ? setName(e.target.value)
                  : setErrMsg("please dont enter @#$%^&*()_")
              }
            />

            <button
              className="  text-capitalize justify-content-center my-2"
              variant="primary"
              onClick={() =>
                name ? setShow(false) : setErrMsg("please enter your name")
              }
            >
              start
            </button>
            <div className="text-danger">{errMsg}</div>
          </form>
        </div>
      ) : (
        <>
          <h4 className="text-capitalize">hello {name} !</h4>
          {/* qui component */}
          <McqComponent />
        </>
      )}
    </div>
  );
}
